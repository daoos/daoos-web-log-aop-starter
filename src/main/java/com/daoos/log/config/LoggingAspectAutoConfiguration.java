package com.daoos.log.config;

import org.springframework.context.annotation.*;

import com.daoos.log.aspect.WebLogAspect;

/**
 * The type Logging aspect auto configuration.
 */
@Configuration
@EnableAspectJAutoProxy
public class LoggingAspectAutoConfiguration {

    /**
     * Logging aspect web log aspect.
     *
     * @return the web log aspect
     */
    @Bean
    public WebLogAspect loggingAspect() {
        return new WebLogAspect();
    }
}
