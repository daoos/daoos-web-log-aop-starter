package com.daoos.log.model;


import java.util.List;

/**
 * Created by daoos on 17-4-17.
 */
public  class UserInfo {
    private String userId;
    private String userName;
    private List userRole;
    private String mobilePhone;

    /**
     * Gets user role.
     *
     * @return the user role
     */
    public List getUserRole() {
        return userRole;
    }

    /**
     * Sets user role.
     *
     * @param userRole the user role
     */
    public void setUserRole(List userRole) {
        this.userRole = userRole;
    }

    /**
     * Gets mobile phone.
     *
     * @return the mobile phone
     */
    public String getMobilePhone() {
        return mobilePhone;
    }

    /**
     * Sets mobile phone.
     *
     * @param mobilePhone the mobile phone
     */
    public void setMobilePhone(String mobilePhone) {
        this.mobilePhone = mobilePhone;
    }

    /**
     * Gets user id.
     *
     * @return the user id
     */
    public String getUserId() {
        return userId;
    }

    /**
     * Sets user id.
     *
     * @param userId the user id
     */
    public void setUserId(String userId) {
        this.userId = userId;
    }

    /**
     * Gets user name.
     *
     * @return the user name
     */
    public String getUserName() {
        return userName;
    }

    /**
     * Sets user name.
     *
     * @param userName the user name
     */
    public void setUserName(String userName) {
        this.userName = userName;
    }
   }
