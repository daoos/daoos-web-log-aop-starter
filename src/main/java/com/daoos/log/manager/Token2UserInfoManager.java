package com.daoos.log.manager;


/**
 * Created by daoos on 17-4-18.
 */
public interface Token2UserInfoManager {
    /**
     * Gets user info by token.
     *
     * @param token the token
     * @return the user info by token
     */
    public  Object getUserInfoByToken(String token);
}
