#daoos-web-log-aop-starter
使用方法：
==============================
1.引入对应的全局日志工具starter
----------------------------
```xml
<dependency>
       <groupId>com.daoos</groupId>
       <artifactId>daoos-web-log-aop-starter</artifactId>
       <version>1.4.5.RELEASE-1.0.6</version>
</dependency>
```
2.使用规则：
----------------------------
###2.0 不在日志中记录用户信息,只记录session和是否登录(不做任何配置)
```json
  {
      "requestInfo": {
          "requestURL": "http://127.0.0.1:8080/api/tokens",
          "requestURI": "/api/tokens",
          "queryString": "username=admin&password=admin",
          "remoteAddr": "127.0.0.1",
          "remoteHost": "127.0.0.1",
          "remotePort": "57240",
          "localAddr": "127.0.0.1",
          "localName": "127.0.0.1",
          "method": "POST",
          "headers": {
              "Origin": "http://127.0.0.1:8080",
              "Authorization": "Bearer null",
              "Cookie": "JSESSIONID=YyVGhedQbnJyrr6f96Pr3NplZsn3_erdDeNk0K3r",
              "Accept": "application/json",
              "User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36",
              "Connection": "keep-alive",
              "Referer": "http://127.0.0.1:8080/swagger-ui/index.html",
              "Host": "127.0.0.1:8080",
              "Accept-Language": "en-US,en;q=0.8,zh-CN;q=0.6,zh;q=0.4",
              "Accept-Encoding": "gzip, deflate, br",
              "Content-Length": "0",
              "Content-Type": "application/json"
          },
          "parameters": {
              "password": [
                  "admin"
              ],
              "username": [
                  "admin"
              ]
          },
          "classMethod": "com.xmjr.app.web.rest.TokenController.login",
          "args": "[admin, admin]"
      },
      "hasLogin": false,
      "sessionId": "BS0Pht214GMOdhZgC8UF42y8avIqElJx8kZtM7sr",
      "referer": "http://127.0.0.1:8080/swagger-ui/index.html",
      "requestTime": "2017-04-18 11:15:59 914",
      "startTime": 1492485359914,
      "endTime": 1492485360113,
      "useTime": 199,
      "url": "/api/tokens",
      "result": {
          "statusCode": "OK",
          "headers": {},
          "body": {
              "code": 100,
              "message": "成功",
              "content": {
                  "userId": "xcvxcv",
                  "terminale": "1",
                  "token": "xcvxcv_1_de469658c080437a800e4cd3991b15fc"
              }
          }
      }
  }
```
###2.1 用户信息在session中存储(通过session中的userkey获取用户信息，输出到日志中)返回的对象必须是UserInfo或继承于UserInfo
 配置为：
```yaml
  logging:
    oauth:
      session:
        userInfoKey: user #默认session中的userKey为user(如果使用的就是user可以不添加此配置)
```
 样例日志为(格式化后)：
```json
  {
      "requestInfo": {
          "requestURL": "http://127.0.0.1:8080/api/tokens",
          "requestURI": "/api/tokens",
          "queryString": "username=admin&password=admin",
          "remoteAddr": "127.0.0.1",
          "remoteHost": "127.0.0.1",
          "remotePort": "57362",
          "localAddr": "127.0.0.1",
          "localName": "127.0.0.1",
          "method": "POST",
          "headers": {
              "Origin": "http://127.0.0.1:8080",
              "Authorization": "Bearer null",
              "Cookie": "JSESSIONID=BS0Pht214GMOdhZgC8UF42y8avIqElJx8kZtM7sr",
              "Accept": "application/json",
              "User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36",
              "Connection": "keep-alive",
              "Referer": "http://127.0.0.1:8080/swagger-ui/index.html",
              "Host": "127.0.0.1:8080",
              "Accept-Language": "en-US,en;q=0.8,zh-CN;q=0.6,zh;q=0.4",
              "Accept-Encoding": "gzip, deflate, br",
              "Content-Length": "0",
              "Content-Type": "application/json"
          },
          "parameters": {
              "password": [
                  "admin"
              ],
              "username": [
                  "admin"
              ]
          },
          "classMethod": "com.xmjr.app.web.rest.TokenController.login",
          "args": "[admin, admin]"
      },
      "userInfo": {
          "username": "liushouquan",
          "password": "admin",
          "id": "xcvxcv",
          "userId": "xcvxcv",
          "mobilePhone": "18610611761"
      },
      "hasLogin": true,
      "userId": "xcvxcv",
      "sessionId": "chnTUAyi_P8rmUQp-fi2DedhM1DYHTFkYviLFHD-",
      "referer": "http://127.0.0.1:8080/swagger-ui/index.html",
      "requestTime": "2017-04-18 11:23:12 664",
      "startTime": 1492485792664,
      "endTime": 1492485792730,
      "useTime": 66,
      "url": "/api/tokens",
      "result": {
          "statusCode": "OK",
          "headers": {},
          "body": {
              "code": 100,
              "message": "成功",
              "content": {
                  "userId": "xcvxcv",
                  "terminale": "1",
                  "token": "xcvxcv_1_3a536d413159479bb76eede4f160a02f"
              }
          }
      }
  }
```
-----------------------------
###2.2 用户信息通过token标识(token请求时取值位置header或参数传递)
  配置为：
```yaml
  logging:
    byToken: true #默认为false,false时不判断token信息
    token:
      tokenKey: Authorization #token对应的key
      tokenKeyInHeader: true #token是否在请求头中,false时从参数中获取token
```
  实现com.daoos.log.manager.Token2UserInfoManager,将token信息转换为用户信息
   返回的对象必须是UserInfo或继承于UserInfo
```java
  /**
   * Created by daoos on 17-4-18.
   */
  @Service
  public class Token2UserInfoServiceImpl  implements Token2UserInfoManager{
      @Autowired
      private TokenManager tokenManager;
      @Override
      public  UserInfo getUserInfoByToken(String var1){
          TokenModel model = tokenManager.getToken(var1);
          if(model!=null){
              User UserInfo=new User();
              UserInfo.setUserId(model.getUserId());
              UserInfo.setMobilePhone("18610611761");
              UserInfo.setUsername("liushoquan");
              List roles=new ArrayList<String>();
              roles.add("role1");
              roles.add("role2");
              UserInfo.setUserRole(roles);
              return UserInfo;
          }else{
              return null;
          }
  
      };
  }
```
  日志结果为：
```json
  {
      "requestInfo": {
          "requestURL": "http://127.0.0.1:8080/api/demo",
          "requestURI": "/api/demo",
          "remoteAddr": "127.0.0.1",
          "remoteHost": "127.0.0.1",
          "remotePort": "59326",
          "localAddr": "127.0.0.1",
          "localName": "127.0.0.1",
          "method": "POST",
          "headers": {
              "Origin": "http://127.0.0.1:8080",
              "Authorization": "xcvxcv_1_285c215238af4731807b7ccda68161e0",
              "Cookie": "JSESSIONID=9yD1Oh4tqDl4ZEQ6K3dNGvkHvUIySpzefEifWx2n",
              "Accept": "application/json",
              "User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36",
              "Connection": "keep-alive",
              "Referer": "http://127.0.0.1:8080/swagger-ui/index.html",
              "Host": "127.0.0.1:8080",
              "Accept-Language": "en-US,en;q=0.8,zh-CN;q=0.6,zh;q=0.4",
              "Accept-Encoding": "gzip, deflate, br",
              "Content-Length": "2",
              "Content-Type": "application/json"
          },
          "parameters": {},
          "classMethod": "com.xmjr.app.web.rest.TokenController.demo",
          "args": "[HttpServletRequestImpl [ POST /api/demo ], null]"
      },
      "userInfo": {
          "username": "liushoquan",
          "userId": "xcvxcv",
          "userRole": [
              "role1",
              "role2"
          ],
          "mobilePhone": "18610611761"
      },
      "hasLogin": true,
      "userId": "xcvxcv",
      "sessionId": "uF33M1J8uEmZ0rpxglJ1j-G5sjA3GvnfXg3aVM1O",
      "token": "xcvxcv_1_285c215238af4731807b7ccda68161e0",
      "referer": "http://127.0.0.1:8080/swagger-ui/index.html",
      "requestTime": "2017-04-18 13:45:35 940",
      "startTime": 1492494335940,
      "endTime": 1492494335951,
      "useTime": 11,
      "url": "/api/demo",
      "result": {
          "statusCode": "OK",
          "headers": {},
          "body": {
              "code": 100,
              "message": "成功",
              "content": {
                  "auth": "xcvxcv_1_285c215238af4731807b7ccda68161e0",
                  "Terminale": "1",
                  "userid": "xcvxcv"
              }
          }
      }
  }
```
