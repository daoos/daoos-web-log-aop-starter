package com.daoos.log.model;

/**
 * <p>
 * 日志对象 <br>
 * </p>
 * <p>
 * -----版本-----变更日期-----责任人-----变更内容<br>
 * ─────────────────────────────────────<br>
 * V3.0.0 2016年6月22日下午3:32:11 liushouquan 初版<br>
 * <p>
 * Copyright (c) 2015，2115,daoos All Rights Reserved. LICENSE INFORMATION
 * </p>
 */
public class WebLogInfo {
    private RequestInfo requestInfo;
    private Object userInfo;
    private String userInfoClassName;
    private boolean hasLogin;
    private String userId;
    private String sessionId;
    private String token;
    private String referer;
    private String requestTime;
    private long startTime;
    private long endTime;
    private long useTime;
    private String ip;
    private String url;
    private String terminale;
    private Object result;
    private String note;

    /**
     * Gets user info class name.
     *
     * @return the user info class name
     */
    public String getUserInfoClassName() {
        return userInfoClassName;
    }

    /**
     * Sets user info class name.
     *
     * @param userInfoClassName the user info class name
     */
    public void setUserInfoClassName(String userInfoClassName) {
        this.userInfoClassName = userInfoClassName;
    }

    /**
     * Is has login boolean.
     *
     * @return the boolean
     */
    public boolean isHasLogin() {
        return hasLogin;
    }

    /**
     * Gets token.
     *
     * @return the token
     */
    public String getToken() {
        return token;
    }

    /**
     * Sets token.
     *
     * @param token the token
     */
    public void setToken(String token) {
        this.token = token;
    }

    /**
     * Gets user info.
     *
     * @return the user info
     */
    public Object getUserInfo() {
        return userInfo;
    }

    /**
     * Sets user info.
     *
     * @param userInfo the user info
     */
    public void setUserInfo(Object userInfo) {
        this.userInfo = userInfo;
    }

    /**
     * Gets has login.
     *
     * @return the has login
     */
    public boolean getHasLogin() {
        return hasLogin;
    }

    /**
     * Sets has login.
     *
     * @param hasLogin the has login
     */
    public void setHasLogin(boolean hasLogin) {
        this.hasLogin = hasLogin;
    }

    /**
     * Gets user id.
     *
     * @return the user id
     */
    public String getUserId() {
        return userId;
    }

    /**
     * Sets user id.
     *
     * @param userId the user id
     */
    public void setUserId(String userId) {
        this.userId = userId;
    }

    /**
     * Gets request info.
     *
     * @return the request info
     */
    public RequestInfo getRequestInfo() {
        return requestInfo;
    }

    /**
     * Sets request info.
     *
     * @param requestInfo the request info
     */
    public void setRequestInfo(RequestInfo requestInfo) {
        this.requestInfo = requestInfo;
    }

    /**
     * Gets session id.
     *
     * @return the session id
     */
    public String getSessionId() {
        return sessionId;
    }

    /**
     * Sets session id.
     *
     * @param sessionId the session id
     */
    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    /**
     * Gets referer.
     *
     * @return the referer
     */
    public String getReferer() {
        return referer;
    }

    /**
     * Sets referer.
     *
     * @param referer the referer
     */
    public void setReferer(String referer) {
        this.referer = referer;
    }

    /**
     * Gets request time.
     *
     * @return the request time
     */
    public String getRequestTime() {
        return requestTime;
    }

    /**
     * Sets request time.
     *
     * @param requestTime the request time
     */
    public void setRequestTime(String requestTime) {
        this.requestTime = requestTime;
    }

    /**
     * Gets start time.
     *
     * @return the start time
     */
    public long getStartTime() {
        return startTime;
    }

    /**
     * Sets start time.
     *
     * @param startTime the start time
     */
    public void setStartTime(long startTime) {
        this.startTime = startTime;
    }

    /**
     * Gets end time.
     *
     * @return the end time
     */
    public long getEndTime() {
        return endTime;
    }

    /**
     * Sets end time.
     *
     * @param endTime the end time
     */
    public void setEndTime(long endTime) {
        this.endTime = endTime;
    }

    /**
     * Gets use time.
     *
     * @return the use time
     */
    public long getUseTime() {
        return useTime;
    }

    /**
     * Sets use time.
     *
     * @param useTime the use time
     */
    public void setUseTime(long useTime) {
        this.useTime = useTime;
    }

    /**
     * Gets ip.
     *
     * @return the ip
     */
    public String getIp() {
        return ip;
    }

    /**
     * Sets ip.
     *
     * @param ip the ip
     */
    public void setIp(String ip) {
        this.ip = ip;
    }

    /**
     * Gets url.
     *
     * @return the url
     */
    public String getUrl() {
        return url;
    }

    /**
     * Sets url.
     *
     * @param url the url
     */
    public void setUrl(String url) {
        this.url = url;
    }

    /**
     * Gets terminale.
     *
     * @return the terminale
     */
    public String getTerminale() {
        return terminale;
    }

    /**
     * Sets terminale.
     *
     * @param terminale the terminale
     */
    public void setTerminale(String terminale) {
        this.terminale = terminale;
    }

    /**
     * Gets result.
     *
     * @return the result
     */
    public Object getResult() {
        return result;
    }

    /**
     * Sets result.
     *
     * @param result the result
     */
    public void setResult(Object result) {
        this.result = result;
    }

    /**
     * Gets note.
     *
     * @return the note
     */
    public String getNote() {
        return note;
    }

    /**
     * Sets note.
     *
     * @param note the note
     */
    public void setNote(String note) {
        this.note = note;
    }
}
