package com.daoos.log.model;

import java.util.Map;

/**
 * Created by daoos on 17-4-17.
 */
public class RequestInfo {
    private String requestURL;
    private String requestURI;
    private String queryString;
    private String remoteAddr;
    private String remoteHost;
    private String remotePort;
    private String localAddr;
    private String localName;
    private String method;
    private Map<String,Object> headers;
    private Map<String,String[]> parameters;
    private String classMethod;
    private String args;

    /**
     * Gets request url.
     *
     * @return the request url
     */
    public String getRequestURL() {
        return requestURL;
    }

    /**
     * Sets request url.
     *
     * @param requestURL the request url
     */
    public void setRequestURL(String requestURL) {
        this.requestURL = requestURL;
    }

    /**
     * Gets request uri.
     *
     * @return the request uri
     */
    public String getRequestURI() {
        return requestURI;
    }

    /**
     * Sets request uri.
     *
     * @param requestURI the request uri
     */
    public void setRequestURI(String requestURI) {
        this.requestURI = requestURI;
    }

    /**
     * Gets query string.
     *
     * @return the query string
     */
    public String getQueryString() {
        return queryString;
    }

    /**
     * Sets query string.
     *
     * @param queryString the query string
     */
    public void setQueryString(String queryString) {
        this.queryString = queryString;
    }

    /**
     * Gets remote addr.
     *
     * @return the remote addr
     */
    public String getRemoteAddr() {
        return remoteAddr;
    }

    /**
     * Sets remote addr.
     *
     * @param remoteAddr the remote addr
     */
    public void setRemoteAddr(String remoteAddr) {
        this.remoteAddr = remoteAddr;
    }

    /**
     * Gets remote host.
     *
     * @return the remote host
     */
    public String getRemoteHost() {
        return remoteHost;
    }

    /**
     * Sets remote host.
     *
     * @param remoteHost the remote host
     */
    public void setRemoteHost(String remoteHost) {
        this.remoteHost = remoteHost;
    }

    /**
     * Gets remote port.
     *
     * @return the remote port
     */
    public String getRemotePort() {
        return remotePort;
    }

    /**
     * Sets remote port.
     *
     * @param remotePort the remote port
     */
    public void setRemotePort(String remotePort) {
        this.remotePort = remotePort;
    }

    /**
     * Gets local addr.
     *
     * @return the local addr
     */
    public String getLocalAddr() {
        return localAddr;
    }

    /**
     * Sets local addr.
     *
     * @param localAddr the local addr
     */
    public void setLocalAddr(String localAddr) {
        this.localAddr = localAddr;
    }

    /**
     * Gets local name.
     *
     * @return the local name
     */
    public String getLocalName() {
        return localName;
    }

    /**
     * Sets local name.
     *
     * @param localName the local name
     */
    public void setLocalName(String localName) {
        this.localName = localName;
    }

    /**
     * Gets method.
     *
     * @return the method
     */
    public String getMethod() {
        return method;
    }

    /**
     * Sets method.
     *
     * @param method the method
     */
    public void setMethod(String method) {
        this.method = method;
    }

    /**
     * Gets headers.
     *
     * @return the headers
     */
    public Map<String,Object> getHeaders() {
        return headers;
    }

    /**
     * Sets headers.
     *
     * @param headers the headers
     */
    public void setHeaders(Map<String,Object> headers) {
        this.headers = headers;
    }

    /**
     * Gets parameters.
     *
     * @return the parameters
     */
    public Map<String,String[]> getParameters() {
        return parameters;
    }

    /**
     * Sets parameters.
     *
     * @param parameters the parameters
     */
    public void setParameters(Map<String,String[]> parameters) {
        this.parameters = parameters;
    }

    /**
     * Gets class method.
     *
     * @return the class method
     */
    public String getClassMethod() {
        return classMethod;
    }

    /**
     * Sets class method.
     *
     * @param classMethod the class method
     */
    public void setClassMethod(String classMethod) {
        this.classMethod = classMethod;
    }

    /**
     * Gets args.
     *
     * @return the args
     */
    public String getArgs() {
        return args;
    }

    /**
     * Sets args.
     *
     * @param args the args
     */
    public void setArgs(String args) {
        this.args = args;
    }
}
