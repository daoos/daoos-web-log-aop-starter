package com.daoos.log.aspect;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import com.daoos.log.model.RequestInfo;
import com.daoos.log.model.UserInfo;
import com.daoos.log.model.WebLogInfo;
import com.daoos.log.manager.Token2UserInfoManager;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.google.gson.Gson;

/**
 * <p>
 * 日志AOP<br>
 * </p>
 * <p>
 * -----版本-----变更日期-----责任人-----变更内容<br>
 * ─────────────────────────────────────<br>
 * V3.0.0 2015年11月3日 liushouquan 初版<br>
 * <p>
 * Copyright (c) 2015，2115,daoos All Rights Reserved. LICENSE INFORMATION
 * </p>
 */
@Aspect
@Configuration
public class WebLogAspect {
    private static final Logger logger = LoggerFactory.getLogger(WebLogAspect.class);

    /**
     * The Start time.
     */
    ThreadLocal<Long> startTime = new ThreadLocal<Long>();
    /**
     * The Gson.
     */
    Gson gson = new Gson();
    @Value("${logging.oauth.session.userInfoKey:user}")
    private String userInfoKey;
    @Value("${logging.oauth.byToken:false}")
    private boolean byToken;
    @Value("${logging.oauth.token.tokenKey:Authorization}")
    private String tokenKey;
    @Value("${logging.oauth.token.tokenKeyInHeader:true}")
    private boolean tokenKeyInHeader;
    @Autowired
    private Token2UserInfoManager token2UserInfoManager;

    /**
     * 返回后通知
     *
     * @param thisJoinPoint 切入点
     * @param retVal        the ret val
     */
    @AfterReturning(pointcut = "webPointcut() target", returning = "retVal")
    public void afterExecuet(JoinPoint thisJoinPoint, Object retVal) {
        // 获取HttpServletRequest
        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder
                .getRequestAttributes();
        HttpServletRequest request = attributes.getRequest();
        WebLogInfo webLogInfo = getWebLogInfo(request, thisJoinPoint);
        webLogInfo.setResult(retVal);
        long endTime = System.currentTimeMillis();
        webLogInfo.setEndTime(endTime);
        webLogInfo.setStartTime(startTime.get());
        webLogInfo.setUseTime(endTime - startTime.get());
        SimpleDateFormat formater = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss S");
        String str = formater.format(new Date(startTime.get()));
        webLogInfo.setRequestTime(str);
        logger.info(gson.toJson(webLogInfo));
    }

    /**
     * 异常时通知
     *
     * @param thisJoinPoint 切入点
     * @param ex            the ex
     */
    @AfterThrowing(pointcut = "webPointcut()", throwing = "ex")
    public void afterThrowing(JoinPoint thisJoinPoint, Exception ex) {
        // ServletRequestAttributes attributes = (ServletRequestAttributes)
        // RequestContextHolder
        // .getRequestAttributes();
        // HttpServletRequest request = attributes.getRequest();
        // logger.error("异常信息" + getWebLogInfo(request, thisJoinPoint));
        logger.error(ex.getMessage());
        logger.error(ex.toString());
    }

    /**
     * Do before.
     *
     * @param joinPoint the join point
     * @throws Throwable the throwable
     */
    @Before("webPointcut()")
    public void doBefore(JoinPoint joinPoint) throws Throwable {
        startTime.set(System.currentTimeMillis());
    }

    /**
     * 定义controller方法切入点
     */
    @Pointcut("within(com..*.*Controller)")
    public void webControllerPointcut() {
    }

    /**
     * 获取请求头信息
     *
     * @param request
     * @return
     */
    private HashMap<String, Object> getHeadersInfo(HttpServletRequest request) {
        HashMap<String, Object> map = new HashMap<String, Object>();
        Enumeration headerNames = request.getHeaderNames();
        while (headerNames.hasMoreElements()) {
            String key = (String) headerNames.nextElement();
            String value = request.getHeader(key);
            map.put(key, value);
        }
        return map;
    }

    /**
     * <p>
     * 获取请求信息<br>
     * </p>
     * <p>
     * -----版本-----变更日期-----责任人-----变更内容<br>
     * ─────────────────────────────────────<br>
     * V1.0.0 2016年6月22日上午10:17:17 liushouquan 初版<br>
     *
     * @param request
     * @param joinPoint
     * @return String
     * @since XMJR V3.0.0
     * </p>
     */
    private RequestInfo getRequestInfo(HttpServletRequest request,
                                       JoinPoint joinPoint) {
        RequestInfo requestInfo = new RequestInfo();
        requestInfo.setRequestURL(request.getRequestURL().toString());
        requestInfo.setRequestURI(request.getRequestURI());
        requestInfo.setQueryString(request.getQueryString());
        requestInfo.setRemoteAddr(request.getRemoteAddr());
        requestInfo.setRemoteHost(request.getRemoteHost());
        requestInfo.setRemotePort(String.valueOf(request.getRemotePort()));
        requestInfo.setLocalAddr(request.getLocalAddr());
        requestInfo.setLocalName(request.getLocalName());
        requestInfo.setMethod(request.getMethod());
        requestInfo.setHeaders(getHeadersInfo(request));
        requestInfo.setParameters(request.getParameterMap());
        requestInfo.setClassMethod(joinPoint.getSignature().getDeclaringTypeName() + "."
                + joinPoint.getSignature().getName());
        requestInfo.setArgs(Arrays.toString(joinPoint.getArgs()));
        return requestInfo;
    }

    /**
     * <p>
     * 获取请求日志信息<br>
     * </p>
     * <p>
     * -----版本-----变更日期-----责任人-----变更内容<br>
     * ─────────────────────────────────────<br>
     * V1.0.0 2016年6月22日上午10:26:23 liushouquan 初版<br>
     *
     * @param request
     * @param thisJoinPoint
     * @return com.xmjr.config.aop.WebLogInfo
     * @since XMJR V3.0.0
     * </p>
     */

    private WebLogInfo getWebLogInfo(HttpServletRequest request, JoinPoint thisJoinPoint) {
        // TODO Auto-generated method stub
        WebLogInfo webLogInfo = new WebLogInfo();
        webLogInfo.setRequestInfo(getRequestInfo(request, thisJoinPoint));
        String className = thisJoinPoint.getTarget().getClass().getName();
        String methodName = thisJoinPoint.getSignature().getName();
        String URL = request.getRequestURI();
        String referer = request.getHeader("referer");
        webLogInfo.setReferer(referer);

        if (byToken) {
//            通过token来存储oauth信息
            String token = null;
            if (tokenKeyInHeader) {
//                tokenKey在请求头的header中
                token = request.getHeader(tokenKey);
            } else {
//                tokenKey在参数中
                token = request.getParameter(tokenKey);
            }
            if (null != token) {
                webLogInfo.setSessionId(request.getSession().getId());
                webLogInfo.setToken(token);
                Object tokenUserInfo = token2UserInfoManager.getUserInfoByToken(token);
                webLogInfo.setUserInfoClassName(tokenUserInfo.getClass().getName());
                if (tokenUserInfo != null && (tokenUserInfo.getClass().getSuperclass() == UserInfo.class||tokenUserInfo.getClass() == UserInfo.class)) {
                    webLogInfo.setUserInfo(tokenUserInfo);
                    UserInfo userInfo = (UserInfo) tokenUserInfo;
                    webLogInfo.setUserId(userInfo.getUserId());
                    webLogInfo.setHasLogin(true);
                }else{
                    webLogInfo.setHasLogin(false);
                }
            } else {
                webLogInfo.setHasLogin(false);
            }
        } else {
//            通过session来存储授信信息
            if (request.getSession() != null) {
                webLogInfo.setSessionId(request.getSession().getId());
                Object cUser = request.getSession().getAttribute(userInfoKey);
                webLogInfo.setUserInfoClassName(cUser.getClass().getName());
                if (cUser != null && (cUser.getClass().getSuperclass() == UserInfo.class||cUser.getClass() == UserInfo.class)) {
                    webLogInfo.setUserInfo(cUser);
                    UserInfo userInfo = (UserInfo) cUser;
                    webLogInfo.setUserId(userInfo.getUserId());
                    webLogInfo.setHasLogin(true);
                } else {
                    webLogInfo.setHasLogin(false);
                }
            } else {
                webLogInfo.setHasLogin(false);
            }
        }
        webLogInfo.setUrl(URL);
        return webLogInfo;
    }

    /**
     * 定义日志全局切入点
     */
    @Pointcut("webControllerPointcut()")
    private void webPointcut() {
    }
}
